# Push Notifications

POC for sending push notifications to a react native app using aws Amplify

### Steps taken to create this POC

Create react native project and install libraries
```sh
npx react-native init
npm install aws-amplify aws-amplify-react-native amazon-cognito-identity-js @aws-amplify/pushnotification
amplify init (use default settings)
```

#### Notes: 
* If react native is not installed : npm install -g react-native-cli
* You will need access to a aws account during amplify init 


### Setup Firebase
* Go to firebase and login or signup a new account
* Add a project
* Enter a project name
* Continue to next page to add firebase to your app
* Click on IOS or Android icon (Will go through the Android setup)
* Register app using applicationId found in android/app/build.gradle in your project
* Follow on screen instructions to dowload and add google-services.json to your project
* Follow on screen instructions to update your android/build.gradle and android/app/build.gradle files

I found the instruction did not provide the following required upate to the android\app\build.gradle file (get latest version)
```sh
implementation 'com.google.firebase:firebase-messaging:20.2.4'
```
* Once setup is completed click on the Gears icon next to Project Overview and select Project Settings
* In the Settings: Cloud Messaging tab, save the Project credentials: Server key for later

###  AWS Amplify Push Notification

```sh
 amplify add notifications
 Select FCM
 For API enter: Server key from above step
```

using this link (https://docs.amplify.aws/lib/push-notifications/getting-started/q/platform/js) follow setps 8 to 11

### Resources
| Topic | Website |
| ------ | ------ |
| Google Firebase | https://console.firebase.google.com |
| React Native | https://reactnative.dev/ |
| AWS Amplify | https://aws-amplify.github.io/docs/js/push-notifications |
| Google Firebase | https://console.firebase.google.com/ |

